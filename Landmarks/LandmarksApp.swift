//
//  LandmarksApp.swift
//  Landmarks
//
//  Created by Erwan Delory on 10/09/2022.
//

import SwiftUI

@main
struct LandmarksApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
